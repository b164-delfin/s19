
const firstNum = 12 ** 3;

console.log(`The cube of 12 is ${firstNum}`);

const address = {
	street: "123 Main St.",
	city: "Los Angeles",
	state: "California",
	zipcode: 91060
}

function getFullAddress({ street, city, state, zipcode}) {
	console.log(`I live in ${street} ${city}, ${state} ${zipcode}.`)
}

getFullAddress(address);


class Animal {
	constructor(kind, kingdom, species, scientificName) {
		this.kind = kind;
		this.kingdom = kingdom;
		this.species = species;
		this.scientificName = scientificName;
	}
}


const myPet = new Animal();

console.log(myPet);

myPet.kind = "dog";
myPet.kingdom = "mammal";
myPet.species = "carnivora";
myPet.scientificName = "Canis Lupus familiaris";

console.log(myPet);

function getPet({ kind, kingdom, species, scientificName}) {
	console.log(`A ${kind} is part of ${kingdom} animal kingdom, it's a ${species} species and the scientific name is ${scientificName}. `)
}

getPet(myPet);

const myOtherPet = new Animal("cat","felidae", "carnivore", "Felis catus");

console.log(myOtherPet);

function getOtherPet({ kind, kingdom, species, scientificName}) {
	console.log(`A ${kind} is part of ${kingdom} animal kingdom, it's a ${species} species and the scientific name is ${scientificName}. `)
}

getOtherPet(myOtherPet);


let Numbers = [ 2, 6, 10, 5, 8];

Numbers.forEach ((number) => {
	return `The number is: ${number}!`
}
)


console.log(Numbers);


const reduceNumber = (Numbers[]) =>


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
		
	}
}

const myDog = new Dog();

console.log(myDog);
